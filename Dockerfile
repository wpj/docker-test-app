FROM golang

ADD . /go/src/bitbucket.org/wpj/docker-test-app

ENV PORT 8080

RUN go get bitbucket.org/wpj/docker-test-app

RUN go-wrapper download
RUN go-wrapper install

# RUN go get ./...

# RUN go install bitbucket.org/wpj/docker-test-app

ENTRYPOINT /go/bin/docker-test-app

EXPOSE ${PORT}

CMD ["go-wrapper", "run"]
