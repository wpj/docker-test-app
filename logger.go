package main

import (
	"log"
	"net/http"
)

// Log handler
type Log struct {
	prefix  string
	handler http.Handler
}

func (l *Log) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Printf("[%s] started %s %s", l.prefix, r.Method, r.URL.Path)
	l.handler.ServeHTTP(w, r)
	log.Printf("[%s] finished %s %s", l.prefix, r.Method, r.URL.Path)
}

// Logger returns new Log instance with default values
func Logger(next http.Handler) http.Handler {
	return &Log{"Log", next}
}
