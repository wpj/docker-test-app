package main

import (
	"log"
	"net/http"
	"os"

	"github.com/justinas/alice"
)

func myHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Write([]byte("{\"foo\": \"bar\"}"))
}

func mySecondHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Write([]byte("{\"bar\": \"baz\"}"))
}

func main() {
	port := os.Getenv("PORT")

	router := New()

	log.Printf("Starting application on port %s.", port)

	router.Handle("GET", "/foo", http.HandlerFunc(myHandler))
	router.Handle("POST", "/foo", http.HandlerFunc(mySecondHandler))

	log.Fatal(http.ListenAndServe(":"+port, alice.New(Logger).Then(router)))

	/*
	 * alternatively:
	 * log.Fatal(http.ListenAndServe(":3000", Logger(router)))
	 */
}
