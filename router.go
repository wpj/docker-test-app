package main

import (
	"net/http"
)

type methodTree map[string]routeTree
type routeTree map[string]http.Handler

// Router provides basic routing functionality
type Router struct {
	Routes methodTree
}

// Handle adds the handler at the path specified
func (rt *Router) Handle(method string, path string, handler http.Handler) {
	if rt.Routes == nil {
		rt.Routes = make(methodTree)
	}
	if rt.Routes[method] == nil {
		rt.Routes[method] = make(routeTree)
	}

	rt.Routes[method][path] = handler
}

// implement http.Handler interface
func (rt *Router) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	handler, ok := rt.Routes[r.Method][r.URL.Path]
	if ok {
		handler.ServeHTTP(w, r)
	}
}

// New returns a pointer to a Router instance
func New() *Router {
	return &Router{}
}
